package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import model.logic.UberData;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	@SuppressWarnings("rawtypes")
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo<>();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					modelo = new MVCModelo<UberData>(); 
					modelo.cargarCSV("./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv");
					System.out.println("Datos de Uber del primer semestre de 2018 cargados");
					System.out.println("Total de viajes reportados:");
					System.out.println("Enero: "+modelo.consultarViajesReportadosMes(01).darTamanoactual());
					System.out.println("Febrero: "+ modelo.consultarViajesReportadosMes(02).darTamanoactual());
					System.out.println("Marzo: "+ modelo.consultarViajesReportadosMes(03).darTamanoactual());
					System.out.println("Abril: "+ modelo.consultarViajesReportadosMes(04).darTamanoactual());
					System.out.println("Mayo: "+ modelo.consultarViajesReportadosMes(05).darTamanoactual());
					System.out.println("Junio: "+ modelo.consultarViajesReportadosMes(06).darTamanoactual());
					break;

				case 2:
					System.out.println("Viajes Reportados en el semestre: "+ modelo.darTamano());								
					break;

				case 3:
					System.out.println("Ingresar n�mero del mes a consultar");
					int mes = lector.nextInt();
						System.out.println("Se reportaron "+ modelo.consultarViajesReportadosMes(mes).darTamanoactual() + " Viajes.");
					double porcentaje = (modelo.consultarViajesReportadosMes(mes).darTamanoactual()/modelo.darTamano())*100;
					System.out.println("Los viajes en el mes n�mero "+mes+" equivalen al "+porcentaje+"% del total de viajes");
					break;
				case 4:
					System.out.println("Ingresar n�mero del mes a consultar");
					int mesOrigen = lector.nextInt();
					System.out.println("Ingresar ID del punto de origen a consultar (Sourceid)");
					int sourceid = lector.nextInt();
					System.out.println("Se reportaron "+ modelo.consultarMesOrigen(mesOrigen, sourceid).darTamanoactual() + " Viajes.");
					double porcentajeOrigen = (modelo.consultarMesOrigen(mesOrigen, sourceid).darTamanoactual()/modelo.darTamano())*100;
					System.out.println("Los viajes en el mes n�mero "+mesOrigen+" y punto de origen #"+sourceid+" equivalen al "+porcentajeOrigen+"% del total de viajes");
					break;

				case 5: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	
					
				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
