package model.logic;

public class UberData {
	//	sourceid: Identificador único de la zona de origen de los viajes relacionados
	//	 dstid: Identificador único de la zona de destino de los viajes relacionados
	//	 month: número del mes de los viajes relacionados
	//	 mean_travel_time: tiempo promedio en segundos de los viajes relacionados
	//	 standard_deviation_travel_time: desviación estándar de los viajes relacionados
	//	 geometric_mean_travel_time: tiempo promedio geométrico en segundos de los viajes
	//	relacionados
	//	 geometric_standard_deviation_travel_time: desviación estándar geométrica de los viajes
	//	relacionados

	private int sourceid;
	private int dstid;
	private int month;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;

	public UberData(int pSourceid,int pDstid,int pMonth,double pMean_travel_time,double pStandard_deviation_travel_time,double pGeometric_mean_travel_time,double pGeometric_standard_deviation_travel_time)
	{
		sourceid = pSourceid;
		dstid = pDstid;
		month = pMonth;
		mean_travel_time = pMean_travel_time;
		standard_deviation_travel_time = pStandard_deviation_travel_time;
		geometric_mean_travel_time = pGeometric_mean_travel_time;
		geometric_standard_deviation_travel_time = pGeometric_standard_deviation_travel_time;
	}

	public int getSourceid() {
		return sourceid;
	}

	public int getDstid() {
		return dstid;
	}

	public int getMonth() {
		return month;
	}

	public double getMean_travel_time() {
		return mean_travel_time;
	}

	public double getStandard_deviation_travel_time() {
		return standard_deviation_travel_time;
	}

	public double getGeometric_mean_travel_time() {
		return geometric_mean_travel_time;
	}

	public double getGeometric_standard_deviation_travel_time() {
		return geometric_standard_deviation_travel_time;
	}
}
