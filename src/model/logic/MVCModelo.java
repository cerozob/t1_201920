package model.logic;

import java.io.FileReader;
import com.opencsv.CSVReader;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.ILista;
import model.data_structures.Node;
import model.data_structures.SimpleLinkedList;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo<T> {
	/**
	 * Atributos del modelo del mundo
	 */
	private IArregloDinamico datos;

	private ILista<T> listaEncadenada;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		listaEncadenada = (ILista<T>) new SimpleLinkedList<T>();
	}

	/**
	 * Constructor del modelo del mundo con capacidad dada
	 * @param tamano
	 */
	public MVCModelo(int capacidad)
	{
		datos = new ArregloDinamico(capacidad);

	}

	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return listaEncadenada.darTamanoactual();
	}


	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */
	public Node<T> buscarNodo(int posicion)
	{
		return listaEncadenada.darNodo(posicion);
	}

	/**
	 * Requerimiento eliminar dato
	 * @param dato Dato a eliminar
	 * @return dato eliminado
	 */
	public Node<T> eliminarNodo(int posicion)
	{
		Node<T> nodoEliminar = listaEncadenada.darNodo(posicion);
		listaEncadenada.eliminarNodo(posicion);
		return nodoEliminar;
	}

	@SuppressWarnings("unchecked")
	public int cargarCSV(String pRutaArchivo)
	{
		int contadorLineas = 0;
		try {
			CSVReader reader = new CSVReader(new FileReader(pRutaArchivo));
			for(String[] linea : reader) 
			{
				
				if(contadorLineas != 0)
				{	
					int pSourceid = Integer.parseInt(linea[0]);
					int pDstid = Integer.parseInt(linea[1]);
					int pMonth = Integer.parseInt(linea[2]);
					double pMean_travel_time = Double.parseDouble(linea[3]);
					double pStandard_deviation_travel_time = Double.parseDouble(linea[4]);
					double pGeometric_mean_travel_time = Double.parseDouble(linea[5]);
					double pGeometric_standard_deviation_travel_time = Double.parseDouble(linea[6]);
					
					UberData dato = new UberData(pSourceid, pDstid, pMonth, pMean_travel_time, pStandard_deviation_travel_time, pGeometric_mean_travel_time, pGeometric_standard_deviation_travel_time);
					Node<UberData> nodo = new Node<UberData>(dato);
					listaEncadenada.agregarNodo((Node<T>) nodo);
				}
				contadorLineas++;
			}
			reader.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return contadorLineas;
	}

	@SuppressWarnings("unchecked")
	public ILista<UberData> consultarViajesReportadosMes(int mes)
	{

		ILista<UberData> listaViajesMensuales = new SimpleLinkedList<UberData>();

		Node<UberData> nodo = (Node<UberData>) listaEncadenada.darNodo(0);

		while (nodo != null)
		{
			
			if(nodo.darObjeto().getMonth() == mes)
			{
				listaViajesMensuales.agregarNodo(nodo);		
				
			}	
			nodo = nodo.darSiguiente();
		
		}
		return listaViajesMensuales;
	}

	public ILista<UberData> consultarMesOrigen(int mes, int sourceid)
	{
		Node<UberData> nodo = consultarViajesReportadosMes(mes).darNodo(0);
		ILista<UberData> listaOrigenMensual = new SimpleLinkedList<UberData>();
		while (nodo != null)
		{
			if(nodo.darObjeto().getSourceid() == sourceid)
			{
				listaOrigenMensual.agregarNodo(nodo);
			}
			nodo = nodo.darSiguiente();
		}

		return listaOrigenMensual;
	}
}
