package model.data_structures;

public class SimpleLinkedList<T> implements ILista<T> {

	private Node<T> primerNodo;	
	private int tamano;

	public SimpleLinkedList() {
		crearListaVacia();
	}

	public void crearListaVacia() {
		primerNodo = null;
		tamano = 0;
	}


	public void agregarNodo(Node<T> nodo) {
		if(primerNodo == null)
		{
			agregarNodoInicio(nodo);
		}
		else
		{
			agregarNodoFinal(nodo);
		}
		tamano++;
	}

	public Node<T> darNodo(int posicion)
	{
		Node<T> nodo = primerNodo;
		int i = 0;
		while(i < posicion+1)
		{
			if(i != posicion && nodo.darSiguiente() != null)
			{
				nodo = nodo.darSiguiente();
				i++;
			}
			else
			{
				return nodo;
			}
		}
		return null;
	}

	public int darTamanoactual() {
		return tamano;
	}


	private void agregarNodoInicio(Node<T> nodo) {
		if(primerNodo != null)
		{
			nodo.setNodoSiguiente(primerNodo);
			primerNodo = nodo;
			
		}
		else
		{
			primerNodo = nodo;
		}
	}

	private void agregarNodoFinal(Node<T> pNodo) 
	{
		Node<T> nodoActual = primerNodo;
		while(nodoActual.darSiguiente() !=null)
		{
			nodoActual = nodoActual.darSiguiente();
		}
		nodoActual.setNodoSiguiente(pNodo);
	}


	private void eliminarPrimerNodo() 
	{
		if(primerNodo != null)
		{
			if(primerNodo.darSiguiente() != null)
			{
				Node<T> nuevoPrimero = primerNodo.darSiguiente();
				primerNodo.setNodoSiguiente(null);
				primerNodo = nuevoPrimero;
			}
			else
			{
				primerNodo = null;
			}
		}
	}


	public void eliminarNodo(int pos) {

		if(pos == 0)
		{
			eliminarPrimerNodo();

		}

		else if(pos == tamano)

		{
			darNodo(pos-1).setNodoSiguiente(null);
		}

		else

		{
			Node<T> nuevoSiguiente = darNodo(pos++);
			darNodo(pos).setNodoSiguiente(null);
			darNodo(pos-1).setNodoSiguiente( nuevoSiguiente );
		}
	}


	public boolean isEmpty() {
		if(tamano == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
