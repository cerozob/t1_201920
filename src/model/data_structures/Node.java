package model.data_structures;

public class Node<T> {

	private Node<T> nodoSiguiente;

	private T objeto;

	public Node (T pObjeto) 
	{
		nodoSiguiente = null;
		objeto = pObjeto;
	}

	public Node<T> darSiguiente() 
	{
		return nodoSiguiente;}

	public void setNodoSiguiente ( Node<T> pSiguiente) 
	{
		nodoSiguiente = pSiguiente;
	}

	public T darObjeto()
	{
		return objeto;
	}

	public void setObjeto (T pObjeto) 
	{
		objeto = pObjeto;
	}

}
