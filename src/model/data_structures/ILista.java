package model.data_structures;


public interface ILista<T> 
{
	public void crearListaVacia();

	public int darTamanoactual( );

	//M�todos para manipular nodos

	public Node<T> darNodo(int posicion);

	public void agregarNodo(Node<T> nodo);

	public void eliminarNodo(int posicion);

	//Metodos para manipular objetos dentro de nodos


	public boolean isEmpty();

}
